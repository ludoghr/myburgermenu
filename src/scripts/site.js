const manageBurgerModeChange = () => {
    $('.burger')
        .off('click')
        .on('click', function() {
            let $burger = $(this);
            if( $('.burger-check').is(':checked') ) {
                $burger.addClass('openMode');
            } else {
                $burger.addClass('closeMode');
            }
            
            setTimeout(function() {
                $burger
                    .removeClass('openMode')
                    .removeClass('closeMode')
            }, 500);
        });
};

$(document).ready( function() {
    manageBurgerModeChange();
});