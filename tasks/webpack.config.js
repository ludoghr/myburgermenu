const path = require('path');
const webpack = require('webpack');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

let configObj = {
  entry: './src/scripts/site.js',
  output: {
    filename: 'site.js',
    path: path.resolve(__dirname, '../bundle')
  },
  plugins: [
    new webpack.ProvidePlugin({
      '$': 'jquery',
      'jQuery': 'jquery',
      'window.jQuery': 'jquery',
      'window.$': 'jquery'
    })
  ],
  module: {
    rules: [
      {
        test: /.jsx?$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      }
    ]
  }
}

module.exports = (env, obj) => {
  if ( obj.mode === 'production' ) {
    configObj.plugins.push(
      new UglifyJsPlugin({
        test: /\.js($|\?)/i,
        parallel: 4
      })
    );
  }
  return configObj;
};